CREATE TABLE if not exists `fp`.`creature` (
  `CreatureID` INT NOT NULL,
  `level` INT NULL,
  `race` INT NULL,
  `basicHP` INT NULL,
  `basicMP` INT NULL,
  `physicalAttack` INT NULL,
  `physicalDefence` INT NULL,
  `magicAttack` INT NULL,
  `magicDefence` INT NULL,
  `dexterity` INT NULL,
  `luck` INT NULL,
  `criticalHit` DOUBLE NULL,
  PRIMARY KEY (`CreatureID`),
  UNIQUE INDEX `CreatureID_UNIQUE` (`CreatureID` ASC),
  INDEX `race_idx` (`race` ASC),
  CONSTRAINT `race`
    FOREIGN KEY (`race`)
    REFERENCES `fp`.`race` (`RaceID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

SELECT * FROM fp.creature;