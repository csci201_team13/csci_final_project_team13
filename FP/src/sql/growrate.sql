CREATE TABLE if not exists `fp`.`growrate` (
  `race` INT NOT NULL,
  `bHP` INT NULL,
  `gHP` DOUBLE NULL,
  `bMP` INT NULL,
  `gMP` DOUBLE NULL,
  `bPhysicalAttack` INT NULL,
  `gPhysicalAttack` DOUBLE NULL,
  `bPhysicalDefence` INT NULL,
  `gPhysicalDefence` DOUBLE NULL,
  `bMagicAttack` INT NULL,
  `gMagicAttack` INT NULL,
  `bMagicDefence`INT NULL,
  `gMagicDefence` INT NULL,
  `bDexterity` INT NULL,
  `gDexterity` DOUBLE NULL,
  `bLuck` INT NULL,
  `gLuck` DOUBLE NULL,
  `bCriticalHit` DOUBLE NULL,
  `gCriticalHit` DOUBLE NULL,
  PRIMARY KEY (`race`),
  UNIQUE INDEX `race_UNIQUE` (`race` ASC));
/*
INSERT INTO `fp`.`growrate` (`race`, `bHP`, `gHP`, `bMP`, `gMP`, `bPhysicalAttack`, `gPhysicalAttack`, `bPhysicalDefence`, `gPhysicalDefence`, `bMagicAttack`, `gMagicAttack`, `bMagicDefence`, `gMagicDefence`, `bDexterity`, `gDexterity`, `bLuck`, `gLuck`, `bCriticalHit`, `gCriticalHit`) VALUES ('0', '200', '1.2', '100', '1.1', '20', '1.2', '5', '1.1', '15', '1.1', '5', '1.1', '5', '1.1', '1', '1.1', '0.02', '1.02');
INSERT INTO `fp`.`growrate` (`race`, `bHP`, `gHP`, `bMP`, `gMP`, `bPhysicalAttack`, `gPhysicalAttack`, `bPhysicalDefence`, `gPhysicalDefence`, `bMagicAttack`, `gMagicAttack`, `bMagicDefence`, `gMagicDefence`, `bDexterity`, `gDexterity`, `bLuck`, `gLuck`, `bCriticalHit`, `gCriticalHit`) VALUES ('1', '100', '1.1', '200', '1.2', '15', '1.1', '3', '1.1', '20', '1.2', '15', '1.2', '6', '1.1', '2', '1.1', '0.01', '1.01');
INSERT INTO `fp`.`growrate` (`race`, `bHP`, `gHP`, `bMP`, `gMP`, `bPhysicalAttack`, `gPhysicalAttack`, `bPhysicalDefence`, `gPhysicalDefence`, `bMagicAttack`, `gMagicAttack`, `bMagicDefence`, `gMagicDefence`, `bDexterity`, `gDexterity`, `bLuck`, `gLuck`, `bCriticalHit`, `gCriticalHit`) VALUES ('2', '300', '1.5', '50', '1.01', '25', '1.3', '3', '1.1', '3', '1.1', '3', '1.1', '10', '1.2', '5', '1.1', '0.03', '1.02');
INSERT INTO `fp`.`growrate` (`race`, `bHP`, `gHP`, `bMP`, `gMP`, `bPhysicalAttack`, `gPhysicalAttack`, `bPhysicalDefence`, `gPhysicalDefence`, `bMagicAttack`, `gMagicAttack`, `bMagicDefence`, `gMagicDefence`, `bDexterity`, `gDexterity`, `bLuck`, `gLuck`, `bCriticalHit`, `gCriticalHit`) VALUES ('3', '350', '1.5', '0', '0', '30', '1.4', '3', '1.1', '0', '0', '0', '0', '2', '1.1', '1', '1.1', '0.01', '1.01');
INSERT INTO `fp`.`growrate` (`race`, `bHP`, `gHP`, `bMP`, `gMP`, `bPhysicalAttack`, `gPhysicalAttack`, `bPhysicalDefence`, `gPhysicalDefence`, `bMagicAttack`, `gMagicAttack`, `bMagicDefence`, `gMagicDefence`, `bDexterity`, `gDexterity`, `bLuck`, `gLuck`, `bCriticalHit`, `gCriticalHit`) VALUES ('4', '50', '1.6', '50', '1.6', '5', '1.5', '5', '1.5', '5', '1.5', '5', '1.5', '20', '1.2', '10', '1.2', '0.05', '1.05');

*/
SELECT * FROM fp.growrate;