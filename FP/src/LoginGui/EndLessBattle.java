package LoginGui;

import java.awt.Font;
import java.util.Properties;

import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.FontUIResource;

import Data.Creature;
import GUI.JClientGui;
import library.FontLibrary;

public class EndLessBattle
{
	public EndLessBattle()
	{
		LoginWindow lw = new LoginWindow();
		Creature creature = lw.getMyCharacter();
		System.out.println(creature.getName());
		lw.dispose();
		if(!lw.isGuest)
		{
			AutoSave as = new AutoSave(creature, lw.getClientListener());
			as.start();
		}
		new JClientGui(creature, lw.getClientListener());
	}
	
	
	public static void main(String[] args)
	{
//		final FontUIResource res = new FontUIResource(FontLibrary.getFont("fonts/Helvetica.ttf", Font.BOLD, 15));
//		UIDefaults defaults = UIManager.getDefaults();
//		defaults.put("Label.font", res);
//		defaults.put("Panel.font", res);
//		defaults.put("TextPane.font", res);
//		defaults.put("TextArea.font", res);
//		defaults.put("Button.font", res);
//		defaults.put("ToggleButton.font", res);
//		defaults.put("TabbedPane.font", res);
		new EndLessBattle();
	}
}
