package LoginGui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Data.Creature;
import Data.EType;
import Data.Equipment;
import Data.Race;
import Data.Rank;
import GUI.flowFrame;
import customUI.PaintedButton;
import library.FontLibrary;
import library.ImageLibrary;

public class SetUpCharacterWindow extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3861365459578777452L;
	
	private Creature newCreature;
	private Race race;
	
	private JButton humanButton;
	private JButton elfButton;
	private JButton dwarfButton;
	private JButton orcButton;
	private JButton undeadButton;
	private JButton confirmButton;
	private JLabel characterNameLabel;
	private JTextField characterNameTextField;
	private JTextArea infoArea;
	private JLabel infoLabel;
	
	private Lock newCharacterLock;
	private Condition newCharacterCondition;
	
	private JTextField promotionCodeTextField;
	
	public SetUpCharacterWindow()
	{

		initializeVariables();
		addActionListeners();

		setGUI();
	}
	
	public void initializeVariables()
	{
		
		humanButton = new JButton("");
		humanButton.setIcon(new ImageIcon(ImageLibrary.getImage("resources/race/Human.png")));
		
		elfButton = new JButton("");
		elfButton.setIcon(new ImageIcon(ImageLibrary.getImage("resources/race/Elf.png")));

		dwarfButton = new JButton("");
		dwarfButton.setIcon(new ImageIcon(ImageLibrary.getImage("resources/race/Dwarf.png")));

		orcButton = new JButton("");
		orcButton.setIcon(new ImageIcon(ImageLibrary.getImage("resources/race/Orc.png")));

		undeadButton= new JButton("");
		undeadButton.setIcon(new ImageIcon(ImageLibrary.getImage("resources/race/Undead.png")));

		
		characterNameLabel = new JLabel("Please input your character name");
		
		confirmButton = new JButton("Confirm");
			
		
		characterNameTextField = new JTextField(20);
		promotionCodeTextField = new JTextField(20);
		newCharacterLock = new ReentrantLock();
		newCharacterCondition = newCharacterLock.newCondition();
		
	}
	
	public void setGUI()
	{
		setLayout(new BorderLayout());
		setSize(500, 300);
		confirmButton.setEnabled(false);
		
		JPanel raceSelectionPanel = new JPanel();
		raceSelectionPanel.setLayout(new GridLayout(5, 1));
		

		raceSelectionPanel.add(humanButton);
		raceSelectionPanel.add(elfButton);
		raceSelectionPanel.add(dwarfButton);
		raceSelectionPanel.add(orcButton);
		raceSelectionPanel.add(undeadButton);
		
		add(raceSelectionPanel, BorderLayout.WEST);
		
		JPanel characterNamePanel = new JPanel();
		characterNamePanel.setLayout(new FlowLayout());
		characterNamePanel.add(characterNameLabel);
		characterNamePanel.add(characterNameTextField);
		
		JPanel promotionPanel = new JPanel();
		promotionPanel.setLayout(new FlowLayout());
		promotionPanel.add(new JLabel("Promotion Code:"));
		promotionPanel.add(promotionCodeTextField);
		
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(2, 1));
		northPanel.add(characterNamePanel);
		northPanel.add(promotionPanel);
		
		add(northPanel, BorderLayout.NORTH);
		add(confirmButton, BorderLayout.SOUTH);
		
		JPanel raceInfoPanel = new JPanel();
		raceInfoPanel.setLayout(new BorderLayout());
		raceInfoPanel.setBorder(new EmptyBorder(180,200,200,200));
		infoArea = new JTextArea();
		infoArea.setFont(FontLibrary.getFont("fonts/Jokerman-Regular.ttf", Font.ITALIC, 15));
		infoArea.setLineWrap(true);
		infoArea.setWrapStyleWord(true);
		infoArea.setOpaque(false);
		infoArea.setEditable(false);
		raceInfoPanel.add(infoArea,BorderLayout.CENTER);
		infoLabel = new JLabel();
		infoLabel.setFont(new Font("Jokerman", Font.BOLD, 30));
		raceInfoPanel.add(infoLabel, BorderLayout.NORTH);
		
		add(raceInfoPanel,BorderLayout.CENTER);
		
		setSize(800,700);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void addActionListeners()
	{
		humanButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				race = Race.Human;
				infoArea.setText("Humans are among the youngest races on Endless Battle"
						+ "With life spans generally shorter "
						+ "than the other races, humans strive all the harder to "
						+ "achieve great heights in exploration.");
				infoLabel.setText("Human");
				confirmButton.setEnabled(true);
				repaint();
			}
		});
		elfButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				race = Race.Elf;
				infoArea.setText("Elfs study the forbidden ways of magic in forest for Years,"
						+ " recognized as born casters. They have highest  Magic damage "
						+ "but lowest Hit points.");
				infoLabel.setText("Elf");
				confirmButton.setEnabled(true);
				repaint();
			}
		});
		dwarfButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				race = Race.Dwarf;
				infoArea.setText("Drawfs dwell in dark underground tunnels, "
						+ "they have highest hit points of all races, but low on vitamin-C "
						+ " gave them relatively low defence.");
				infoLabel.setText("Drawf");
				confirmButton.setEnabled(true);
				repaint();
			}
		});
		orcButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				race = Race.Orc;
				infoArea.setText("Orcs are the physical race, with no knowledge about"
						+ " magic or whatsoever, no magic damage or magic defence,  "
						+ "high physic attack and hit-points.");
				infoLabel.setText("Orc");
				confirmButton.setEnabled(true);
				repaint();
			}
		});
		undeadButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e)
			{
				race = Race.Undead;
				infoArea.setText("Once dead then revived to the world of"
						+ " the living, their initial stats are all very low, but their stat "
						+ "increment on level-up are the highest. They also have "
						+ "highest luck among all");
				infoLabel.setText("Undead");
				confirmButton.setEnabled(true);
				repaint();
			}
		});
		
		confirmButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(characterNameTextField.getText().isEmpty())
				{
					return;
				}
				newCharacterLock.lock();

				newCreature = Creature.generateCreature(race);
				
				newCreature.setName(characterNameTextField.getText());
				
				String promotionCode = promotionCodeTextField.getText();
				if ("alwaysflush".equalsIgnoreCase(promotionCode)) {
					Random rand = new Random();
					Equipment pEquipment = Equipment.generateEquipment(Rank.legendary, EType.values()[Math.abs(rand.nextInt(EType.values().length))]);
					for (int i=0; i<5;++i) {
						pEquipment.upgrade();
					}
					newCreature.addEquipment(pEquipment);
					newCreature.addGold(10000);
					JOptionPane.showMessageDialog(null, "Always Flush!\n    -----Prof. Miller");
				}
				newCharacterCondition.signal();
				newCharacterLock.unlock();
				SetUpCharacterWindow.this.dispose();
			}
		});
	}
	
	public Creature getNewCharacter()
	{
		newCharacterLock.lock();
		
		while(newCreature == null)
		{
			try
			{
				newCharacterCondition.await();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		newCharacterLock.unlock();
		return newCreature;
	}
	
}
