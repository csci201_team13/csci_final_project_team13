package LoginGui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class JGifPane extends JFrame {
	private static final long serialVersionUID = -411832170350923396L;
	private ActionListener myAction;

	public JGifPane(ActionListener action) throws MalformedURLException {
		URL url = new URL("http://i.imgur.com/6UEK2B5.gif");
		Icon icon = new ImageIcon(url);
		// BufferedImage scaledImage = Scalr.resize(icon, 200);
		JLabel label = new JLabel(icon);

		myAction = action;
		setSize(new Dimension(600, 400));
		add(label);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setAlwaysOnTop(true);
	}
	
	
	public void dispose()
	{
		JButton button = new JButton();
		button.addActionListener(myAction);
		button.doClick();
		super.dispose();
	}
}