package LoginGui;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import javax.swing.JOptionPane;
import Data.Creature;
import GUI.Leaderboard;
import Server.Constants;
import Server.Signal;

public class ClientListener extends Thread
{
	private Socket s;
	private ObjectOutputStream	oos;
	private ObjectInputStream	ois;
	
	private LoginWindow lw;
	
	
	public ClientListener(LoginWindow lw)
	{
		this.lw = lw;

		boolean socketReady = initializeVariables();
		
		if(socketReady)
			start();
	}
	
	public boolean initializeVariables()
	{
		try
		{
//			this.s =  new Socket("localhost", 6789);
			this.s =  new Socket("104.236.143.142", 6789);
//			this.s =  new Socket("ec2-54-183-193-30.us-west-1.compute.amazonaws.com", 6789);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			this.oos = new ObjectOutputStream(s.getOutputStream());
			oos.flush();
			this.ois = new ObjectInputStream(s.getInputStream());
			
		} catch (IOException ioe) {
			System.out.println("IOE in ChatThread constructor: " + ioe.getMessage());
			return false;
		}
		
		return true;
	}
	
	public void sendObject(Object obj)
	{
		if(lw.isGuest)
		{
			return;
		}
		
		try
		{
			oos.writeObject(obj);
			oos.flush();
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void run()
	{
		Object obj = null;
		try
		{
			while(true)
			{
				obj = ois.readObject();
				if(obj instanceof Signal)
				{
					Signal signal = (Signal)obj;
					System.out.println(signal.getMessage());
					if(signal.equals(Constants.LOGINSUCCESS))
					{
						lw.setErrorMessage("");
						obj = ois.readObject();
						if(obj instanceof Creature)
						{
							Creature character = (Creature)obj;
							lw.setMyCharacter(character);
						}
						else if(obj instanceof Signal)
						{
							Signal s = (Signal)obj;
							if(s.equals(Constants.CANNOTFINDCHARACTER))
							{
								SetUpCharacterWindow setupWindow = new SetUpCharacterWindow();
								Creature character = setupWindow.getNewCharacter();
								sendObject(Constants.CREATNEWCHARACTER);
								sendObject(character);
							}
						}
					}
					else if(signal.equals(Constants.AUTHENTICATEFAIL) || signal.equals(Constants.USERNOTFOUND))
					{
//						JOptionPane.showMessageDialog(null, );
						lw.setErrorMessage("Wrong Password or Invalid Username");
					}
					else if(signal.equals(Constants.REGISTERSUCCESS))
					{
						JOptionPane.showMessageDialog(null, "Register Successfully");
						SetUpCharacterWindow setupWindow = new SetUpCharacterWindow();
						Creature character = setupWindow.getNewCharacter();
						sendObject(Constants.CREATNEWCHARACTER);
						sendObject(character);
						lw.setErrorMessage("");
						lw.setMyCharacter(character);
					}
					else if(signal.equals(Constants.USERNAMEOCCUPIED))
					{
						lw.setErrorMessage("Username already occupied");
					}
					else if(signal.equals(Constants.LEADERBOARD))
					{
						obj = ois.readObject();
						if(obj instanceof Vector<?>)
						{
							Vector<Creature> list = (Vector<Creature>)(obj);
							Leaderboard ldbd = new Leaderboard(list);
						}
					}
				}
			}
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
}