package LoginGui;


import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import customUI.PaintedButton;
import customUI.PaintedPanel;
import Data.Creature;
import library.FontLibrary;
import library.ImageLibrary;
import Server.Constants;
import Server.LoginInfo;

public class LoginWindow extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6213688850588427879L;
	JLabel usernameLabel;
	JLabel passwordLabel;
	JTextField usernameTextField;
	JPasswordField passwordTextField;
	PaintedButton loginButton;
	PaintedButton registerButton;
	PaintedButton tryButton;
	PaintedButton exitButton;
	
	JLabel errorMessageLabel;
	
	ClientListener cc;
	
	private Creature myself = null;
	private Lock loginLock;
	private Condition loginCondition;
	
	private Font font;
	
    Point mouseDownCompCoords;
    
    public boolean isGuest = false;

	
	public LoginWindow()
	{
		initializeVariables();
		createGUI();
		addActionListeners();
		
		setLocation(300, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(300,400);
		setMaximumSize(getSize());
		setMinimumSize(getSize());
		setUndecorated(true);
		setVisible(true);
		
	}
	
	
	public void initializeVariables()
	{
		cc = new ClientListener(this);
		
		font = FontLibrary.getFont("./resources/fonts/kenvector_future_thin.ttf", Font.PLAIN, 20);

		loginLock = new ReentrantLock();
		loginCondition = loginLock.newCondition();
		
		
		
		usernameLabel = new JLabel("Username");
		usernameLabel.setFont(font);

		passwordLabel = new JLabel("Password");
		passwordLabel.setFont(font);
		
		usernameTextField = new JTextField(20);
		passwordTextField = new JPasswordField(20);
		
//		loginButton = new JButton(" Log in ");
		loginButton = new PaintedButton(" Log in ", ImageLibrary.getImage("resources/buttons/transparent_button.png"), 
				ImageLibrary.getImage("resources/buttons/transparent_button.png"), 20);
		registerButton = new PaintedButton("Register", ImageLibrary.getImage("resources/buttons/transparent_button.png"), 
				ImageLibrary.getImage("resources/buttons/transparent_button.png"), 20);
		tryButton = new PaintedButton(" Guest ", ImageLibrary.getImage("resources/buttons/transparent_button.png"), 
				ImageLibrary.getImage("resources/buttons/transparent_button.png"), 20);
		exitButton = new PaintedButton("Exit", ImageLibrary.getImage("resources/buttons/transparent_button.png"), 
				ImageLibrary.getImage("resources/buttons/transparent_button.png"), 20);
				
				

		errorMessageLabel = new JLabel();
		errorMessageLabel.setForeground(Color.RED);
		

	}
	
	public void createGUI()
	{
		loginButton.setFont(font);
		registerButton.setFont(font);
		tryButton.setFont(font);
		exitButton.setFont(font);

		JPanel contendPane = new PaintedPanel(ImageLibrary.getImage("resources/background/colored_shroom.png"));
		contendPane.setLayout(new GridBagLayout());
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridwidth = 2;
		gbc.gridx = 0;
		gbc.gridy = 0;
		contendPane.add(usernameLabel, gbc);
		gbc.gridy = 1;
		contendPane.add(usernameTextField, gbc);
		gbc.gridy = 2;
		contendPane.add(passwordLabel, gbc);
		
		gbc.gridy = 3;
		contendPane.add(passwordTextField, gbc);
		

		
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.weightx = 0.5;
		contendPane.add(loginButton, gbc);
		gbc.gridx = 1;
		contendPane.add(registerButton, gbc);
		
		gbc.gridy = 5;
		gbc.gridx = 0;
		contendPane.add(tryButton, gbc);
		gbc.gridx = 1;
		contendPane.add(exitButton, gbc);
		
		
		
		gbc.gridy = 6;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		gbc.anchor = GridBagConstraints.BASELINE;
		contendPane.add(errorMessageLabel, gbc);
		
		setContentPane(contendPane);
	}
	
	public void addActionListeners()
	{
		loginButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String username = usernameTextField.getText();
				@SuppressWarnings("deprecation")
				String password = passwordTextField.getText();
								
				if(username.isEmpty() || password.isEmpty())
				{
					errorMessageLabel.setText("Empty Username or Password");
//					LoginWindow.this.pack();
					LoginWindow.this.revalidate();
					LoginWindow.this.repaint();
					
					System.out.println("Error");

					return;
				}
				
				LoginInfo login = new LoginInfo(username, password);
				cc.sendObject(Constants.LOGIN);
				cc.sendObject(login);
			}
		});
		
		
		registerButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String username = usernameTextField.getText();
				@SuppressWarnings("deprecation")
				String password = passwordTextField.getText();
								
				if(username.isEmpty() || password.isEmpty())
				{
					errorMessageLabel.setText("Empty Username or Password");
//					LoginWindow.this.pack();
					LoginWindow.this.revalidate();
					LoginWindow.this.repaint();
					
					return;
				}
				
				LoginInfo login = new LoginInfo(username, password);
				cc.sendObject(Constants.REGISTER);
				cc.sendObject(login);
			}
		});
		
		
		tryButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SetUpCharacterWindow setup = new SetUpCharacterWindow();
				Thread t = new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						myself = setup.getNewCharacter();	
						setMyCharacter(myself);
					}
				});
				t.start();
				isGuest = true;
			}
		});
		
		exitButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		
		addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            }
        });

        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
            }
        });
	}
	
	public void setErrorMessage(String errorMessage)
	{
		errorMessageLabel.setText(errorMessage);
		this.revalidate();
		this.repaint();
	}
	
	public Creature getMyCharacter()
	{
		loginLock.lock();
		while(myself == null)
		{
			try
			{
				loginCondition.await();
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		loginLock.unlock();
		
		return myself;
	}
	
	public void setMyCharacter(Creature c)
	{
		System.out.println("Set character");
		loginLock.lock();
		myself = c;
		loginCondition.signalAll();
		loginLock.unlock();
	}
	
	public ClientListener getClientListener()
	{
		return cc;
	}
}


