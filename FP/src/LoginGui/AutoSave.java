package LoginGui;

import java.io.IOException;

import Data.Creature;
import Server.Constants;
import library.Serializer;

public class AutoSave extends Thread
{
	private Creature creature;
	private ClientListener mListener;
	
	public AutoSave(Creature c, ClientListener cc)
	{
		creature = c;
		mListener = cc;
	}
	
	
	public void run()
	{
		while(true)
		{
			
//			System.out.println("t");
			
			try
			{

				Thread.sleep(10000);
				
//				System.out.println(creature.getGold());
//				System.out.println(creature.getEquipments()[0]);
				
				mListener.sendObject(Constants.SAVECHARACTER);
				mListener.sendObject(Serializer.serialize(creature));
				
			}
			catch (InterruptedException | IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
}
