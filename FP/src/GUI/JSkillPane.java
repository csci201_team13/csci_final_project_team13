package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.Border;

import Data.Skill;
import Game.GameManager;

public class JSkillPane extends JPanel {
	private JToggleButton activate;
	private Skill thisSkill;
	private Border title;
	private JPanel upper;
	private JPanel dis;
	private JLabel description;
	private JPanel button;
	private JButton upgrade;
	private GameManager gamem;

	public JSkillPane(Skill s, JClientGui jClientGui, GameManager gm) {
		
		gamem = gm;
		thisSkill = s;
		title = BorderFactory.createLineBorder(Color.white);
		setBorder(title);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		activate = new JToggleButton("activate");
		if (s.IsActivated())
			activate.setSelected(true);

		upper = new JPanel();
		dis = new JPanel();
		upper.setPreferredSize(new Dimension(390, 80));
		dis.setPreferredSize(new Dimension(390, 50));

		button = new JPanel();
		upgrade = new JButton("upgrade (" + thisSkill.getUpgradeAP() + " AP needed)");
		description = new JLabel(thisSkill.getDiscription());
		dis.add(description);
		upper.add(new JLabel("lv." + thisSkill.getLevel() + " " + thisSkill.getName()), BorderLayout.WEST);
		button.add(activate);
		button.add(upgrade);

		// add(dis);
		add(upper);
		add(button);

		upgrade.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				gamem.upgradeSkill(thisSkill);

			}

		});

		setPreferredSize(new Dimension(390, 70));
		activate.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					thisSkill.setActivated(true);
				} else {
					thisSkill.setActivated(false);
				}
				jClientGui.repaint();
				jClientGui.revalidate();
			}

		});
	}

	public void paintComponent(Graphics g) {

	}
}
