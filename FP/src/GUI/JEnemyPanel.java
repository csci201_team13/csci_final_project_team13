package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.Border;

import Data.Creature;

public class JEnemyPanel extends JPanel{
	Creature thisEnemy = null;

	private GridBagConstraints gbc;
	private Border title;
//	private JTextPane enemyInfo;
	private JLabel labels;
	private JLabel enemyInfoLabel;
	private JLabel nameLabel;
	private JLabel hpLabel;
	private JLabel mpLabel;
	private JLabel nl;
	private JLabel hl;
	private JLabel ml;
	
	
	public JEnemyPanel(){
		title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Enemy");
		setBorder(title);
		setLayout(new GridLayout(3, 2));

		nameLabel = new JLabel("Name: ");
		hpLabel = new JLabel("HP: ");
		mpLabel = new JLabel("MP: ");
		nl = new JLabel("");
		hl = new JLabel("");
		ml = new JLabel("");
		
		
		add(nameLabel);
		add(nl);
		add(hpLabel);
		add(hl);
		add(mpLabel);
		add(ml);
//		setPreferredSize(new Dimension(200, 100));
		
	}
	
	
	public void setEnemy(Creature e){
		thisEnemy = e;
		
	}
	
	private void refresh()
	{
		nl.setText("<html>" + thisEnemy.getName() + "</html>");
		hl.setText(""+thisEnemy.getHPCurrent());
		ml.setText(""+thisEnemy.getMPCurrent());
	}
	
	public void paintComponent(Graphics g){
		refresh();
		super.paintComponent(g);
		
	}
}
