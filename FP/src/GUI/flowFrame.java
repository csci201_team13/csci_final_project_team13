package GUI;

import java.util.PriorityQueue;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class flowFrame extends JFrame {
	private static final long serialVersionUID = -4804195141757047140L;
	public static Vector<flowFrame> frameList = new Vector<>();
	private JPanel jp;
	public flowFrame(JPanel jp) {
		this.jp = jp;
		setVisible(false);
		setSize(jp.getSize());
		setUndecorated(true);
		add(jp);
		
		frameList.add(this);
	}
}
