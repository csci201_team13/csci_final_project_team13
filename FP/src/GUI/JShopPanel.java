package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Random;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ScrollPaneLayout;

import Data.EType;
import Data.Equipment;
import Data.Rank;
import Game.GameManager;

public class JShopPanel extends JDialog
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8265857000016950131L;
	private GameManager gm;
	private JClientGui gui;
	private JScrollPane jsp;
	private JPanel itemsPanel;
	
	private Vector<Equipment> itemList;
	public JShopPanel(GameManager gm, JClientGui gui)
	{
		this.gm = gm;
		this.gui = gui;
		setLayout(new BorderLayout());
		setSize(new Dimension(400, 700));
		setPreferredSize(getSize());
		setResizable(false);
		itemsPanel = new JPanel(new GridLayout(6,1));
		initialize();

		
		
		jsp = new JScrollPane(itemsPanel);
		setContentPane(jsp);
		jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
//		add(jsp);

	}
	
	private void initialize()
	{
		itemList = new Vector<>(4);
		Vector<JShopItem> items = new Vector<>();
		
		Rank[] rank = Rank.values();
		EType[] etype = EType.values();
		
		Random rand = new Random();
		int level = gm.getPlayer().getLevel();
		
		for(int i = 0; i < 6; i++)
		{
			
			Equipment e = Equipment.generateEquipment(rank[(int)(Math.random() * 5)], etype[rand.nextInt(3)]);
			for(int j = 0; j < (int)(level * 2 * Math.random()); j++)
			{
				e.upgrade();
			}
			
			itemList.add(e);

			items.add(new JShopItem(itemList.get(i), gm.getPlayer(),gui, gm, this));
		}
		
		
		for(int i = 0; i < items.size(); i++)
		{
			itemsPanel.add(items.get(i));
		}
	}
	
	
	private void update()
	{

		itemsPanel.removeAll();

		Vector<JShopItem> items = new Vector<>();

		for(int i = 0; i < itemList.size(); i++)
		{	
			items.add(new JShopItem(itemList.get(i), gm.getPlayer(),gui, gm, this));
		}

		for(int i = 0; i < items.size(); i++)
		{
			itemsPanel.add(items.get(i));
		}
		
		revalidate();
		repaint();
		System.out.println("Update shop");


	}
	
	public void buy(Equipment e)
	{
		System.out.println(itemList.size());
		itemList.remove(e);
		System.out.println(itemList.size());

		update();
	}
}



