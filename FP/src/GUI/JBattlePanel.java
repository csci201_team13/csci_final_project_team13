package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import Data.Equipment;

public class JBattlePanel extends JPanel{
	//private JTextArea jta;
	private JTextPane jtp;
	private JScrollPane s;
	
	public JBattlePanel(){
//		jta = new JTextArea(30,30);
//		jta.setEditable(false);
//		s = new JScrollPane(jta);
		
		//////////////////////////
		// set fixed size of jtp//
		//////////////////////////
		setPreferredSize(new Dimension(400, 700));
		setLayout(new GridLayout(1, 1));
		jtp = new JTextPane();
		jtp.setEditable(false);
		jtp.setBackground(new Color(238, 238, 238));
		s = new JScrollPane(jtp);
		s.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener(){
			@Override
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
				
			}
			
		});
		
		add(s);
	}
	
	public void printLog(String input){
//		jta.append(input);
//		jta.append("\n");
		append(input, Color.black);
	}
	
	public void printAttack(String attacker, String defender, int dmg){
//		jta.setForeground(Color.orange);
//		jta.append(attacker);
//		jta.setForeground(Color.black);
//		jta.append(" caused " + dmg + " to ");
//		jta.setForeground(Color.orange);
//		jta.append(defender);
//		jta.setForeground(Color.black);
//		jta.append("\n");
		synchronized (this)
		{
			append(attacker, Color.orange);
			append(" casused ", Color.black);
			append(dmg+"", Color.red);
			append(" to ", Color.black);
			append(defender+"\n", Color.cyan);
		}

	}
	
	public void youDie(){
//		jta.append("You Die");
//		jta.append("\n");
		synchronized (this)
		{
			append("You die\n", Color.black);
		}
	}
	
	public void youWin(String name){
//		jta.append("You Defeated " + name + "\n");
		synchronized (this)
		{
			append("You defeated ", Color.black);
			append(name+"\n", Color.cyan);
		}
	}
	
	public void printMeet(String name){
//		jta.append("You meet " + name + "\n");
		synchronized (this)
		{
			append("You meet ", Color.black);
			append(name+"\n", Color.cyan);
		}

	}
	
	public void printExp(int Exp){
//		jta.append("You get " + Exp + "\n");
		synchronized (this)
		{
			append("You get "+Exp +" exp\n", Color.black);
		}
	}
	
	public void printMoney(int money){
//		jta.append("You earn $ " + money + "\n");
		synchronized (this)
		{
			append("You get " + money+" gold\n", Color.black);
		}
	}
	
	public void printLoot(Equipment loot){
//		jta.append("You get " + loot + "\n");
		
		synchronized (this)
		{
			Color color = Color.black;
			switch (loot.getRank().ordinal()) {
			case 0:
			case 1:
				color = Color.black;
				break;
			case 2:
				color = Color.green;
				break;
			case 3:
				color = Color.pink;
				break;
			case 4:
				color = Color.red;
				break;
			default:
				break;
			}
			append("You get ", Color.black);
			append(loot.getName() + "\n", color);
		}
	}
	
	public void printUseSkill(String name, String skill){
//		jta.append(name + " used " + skill + "\n");
		
		synchronized (this)
		{
			append(name + " used ", Color.black);
			Random rand = new Random();
			append(skill+"\n", new Color(Math.abs(rand.nextInt(255)), Math.abs(rand.nextInt(255)), Math.abs(rand.nextInt(255))));
		}
	}
	
	private void append(String str, Color color) {
		SimpleAttributeSet sas = new SimpleAttributeSet();
		StyleConstants.setForeground(sas, color);
		StyledDocument sdoc = jtp.getStyledDocument();
		try {
			sdoc.insertString(sdoc.getLength(), str, sas);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
		//jtp.setCaretPosition(jtp.getDocument().getLength());
	}
}
