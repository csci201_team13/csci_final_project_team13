package GUI;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Data.Creature;
import Data.Skill;
import Game.GameManager;

public class JSkillTotalPane extends JPanel{
	private JSkillPane[] pane; 
	private Set<Skill> posSkills;
	private Set<Skill> negSkills;
	private Creature thisPlayer;
	private JScrollPane scroll;
	private GameManager gamem;
	
	private JClientGui jClientGui;
	
	public JSkillTotalPane(Creature p, JClientGui jClientGui, GameManager gm){
		this.jClientGui = jClientGui;
		
		gamem = gm;
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(380,800));
		thisPlayer = p;
		posSkills = thisPlayer.getPositiveSkills();
		negSkills = thisPlayer.getPassiveSkills();
		Iterator<Skill> setIt = posSkills.iterator();
		pane = new JSkillPane[posSkills.size() + negSkills.size()];
		int counter = 0;

		while(setIt.hasNext()){
			pane[counter] = new JSkillPane(setIt.next(),jClientGui,gamem);
			scroll = new JScrollPane(pane[counter]);
			add(scroll);
			counter++;
		}
		
		Iterator<Skill> setIt2 = negSkills.iterator();
		while(setIt2.hasNext()){
			pane[counter] = new JSkillPane(setIt2.next(),jClientGui,gamem);
			scroll = new JScrollPane(pane[counter]);
			add(scroll);
			counter++;
		}
	
		
		
	}
	
	public void update(){
		removeAll();
		
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(380,800));
		posSkills = thisPlayer.getPositiveSkills();
		negSkills = thisPlayer.getPassiveSkills();
		Iterator<Skill> setIt = posSkills.iterator();
		pane = new JSkillPane[posSkills.size() + negSkills.size()];
		int counter = 0;

		while(setIt.hasNext()){
			pane[counter] = new JSkillPane(setIt.next(),jClientGui,gamem);
			scroll = new JScrollPane(pane[counter]);
			add(scroll);
			counter++;
		}
		
		Iterator<Skill> setIt2 = negSkills.iterator();
		while(setIt2.hasNext()){
			pane[counter] = new JSkillPane(setIt2.next(),jClientGui,gamem);
			scroll = new JScrollPane(pane[counter]);
			add(scroll);
			counter++;
		}
	
		revalidate();
		repaint();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}
}
