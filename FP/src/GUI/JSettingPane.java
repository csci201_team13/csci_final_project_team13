package GUI;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.sql.Savepoint;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import Data.Creature;
import Game.GameManager;
import LoginGui.ClientListener;
import Server.Constants;
import library.Serializer;

public class JSettingPane extends JPanel
{
	private static final long serialVersionUID = -7002400775206945606L;
	ClientListener mListener;
	private JButton savingButton;
	private Creature mCreature;
	private JToggleButton pvpButton;
	private JButton leaderBoardButton;
	private JButton shopButton;
	private JButton exitButton;
	private JButton rebirthButton;
	private GameManager gm;
	
	
	private JClientGui mainGUI;
	public JSettingPane(ClientListener cl, Creature c, JClientGui gui)
	{
		mainGUI = gui;
		mListener = cl;
		mCreature = c;
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		savingButton = new JButton("Save");
		savingButton.setPreferredSize(new Dimension(180,40));
		add(savingButton,gbc);
	
		gbc.gridy = 1;
		pvpButton = new JToggleButton("PVP");
		pvpButton.setPreferredSize(new Dimension(180,40));
		add(pvpButton, gbc);
		
		gbc.gridy = 2;
		leaderBoardButton = new JButton("Leader Board");
		leaderBoardButton.setPreferredSize(new Dimension(180,40));
		add(leaderBoardButton, gbc);
		
		gbc.gridy = 3;
		shopButton = new JButton("Shop");
		shopButton.setPreferredSize(new Dimension(180,40));
		add(shopButton, gbc);
		
		gbc.gridy = 4;
		rebirthButton = new JButton("Rebirth");
		rebirthButton.setPreferredSize(new Dimension(180,40));
		add(rebirthButton, gbc);
		
		gbc.gridy = 5;
		exitButton = new JButton("Exit");
		exitButton.setPreferredSize(new Dimension(180,40));
		add(exitButton, gbc);
		
		savingButton.addActionListener(new ActionListener()
		{			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				mListener.sendObject(Constants.SAVECHARACTER);
				byte[] data;
				try
				{
					data = Serializer.serialize(mCreature);
					mListener.sendObject(data);
				}
				catch (IOException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		pvpButton.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					gm.setPvpFlag(true);
				} else {
					gm.setPvpFlag(false);
				}
			}
		});
		
		leaderBoardButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				mListener.sendObject(Constants.LEADERBOARD);
			}
		});
		
		shopButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				JShopPanel shop = new JShopPanel(gm, gui);
				shop.setVisible(true);
			}
		});
		
		
		rebirthButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// TODO Auto-generated method stub
				c.setLevel(1);
			}
		});
		
		
		exitButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
	}
	public void setGameManager(GameManager gManager) {
		gm = gManager;
	}
}
