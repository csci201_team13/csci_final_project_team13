package GUI;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JGifPane extends JPanel {
	public static String[] giflist = { "http://i.imgur.com/RPn60th.gif", "http://i.imgur.com/Dii79OA.gif",
			"http://i.imgur.com/Vfty5c1.gif", "http://i.imgur.com/3Zz1t0y.gif", "http://i.imgur.com/JsSbTSR.gif",
			"http://i.imgur.com/YR2BHKp.gif",

	};
	private JLabel newLabel;
	private int count;
	private static final int countMax = 5;
	private Random rand;
	public JGifPane() throws MalformedURLException {
		count = countMax;
		
		rand = new Random();
		URL url = new URL(giflist[rand.nextInt(6)]);
		Icon icon = new ImageIcon(url);
		// BufferedImage scaledImage = Scalr.resize(icon, 200);
		JLabel label = new JLabel(icon);

		setSize(new Dimension(380, 200));
		add(label);
		setVisible(true);
	}

	public void swapImage() throws MalformedURLException {
		if (--count > 0) {
			return;
		}
		count = countMax;
		
		URL url = new URL(giflist[rand.nextInt(6)]);
		Icon icon = new ImageIcon(url);
		newLabel = new JLabel(icon);
		this.setSize(new Dimension(600, 800));
		this.add(newLabel);
		this.setComponentZOrder(newLabel, 0);
		this.setVisible(true);
		this.revalidate();
		this.repaint();

	}
}