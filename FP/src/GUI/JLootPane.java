package GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Data.Rank;

public class JLootPane extends JPanel{
	private int totalGold;
	private int totalExp;
	private Border title;
	private int broken;
	private int basic;
	private int majic;
	private int rare;
	private int legendary;
	
	public JLootPane(){
		
		broken = 0;
		basic = 0;
		majic = 0;
		rare = 0;
		legendary = 0;
		
		totalGold = 0;
		totalExp = 0;
		title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"MAP LOOT");
		setBorder(title);
		
	}
	
	public void addGold(int g){
		totalGold += g;
	}
	
	public void addExp(int e){
		totalExp += e;
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.drawString("Gold: ", 10,40);
		g.setColor(Color.black);
		g.drawString(Integer.toString(totalGold), 100, 40);
		g.setColor(Color.blue);
		g.drawString("EXP: ", 10, 60);
		g.setColor(Color.black);
		g.drawString(Integer.toString(totalExp), 100, 60);
		g.setColor(Color.gray);
		g.drawString("Broken: ", 10, 80);
		g.setColor(Color.black);
		g.drawString(Integer.toString(broken), 100, 80);
		g.setColor(Color.blue);
		g.drawString("Basic: ", 10, 100);
		g.setColor(Color.black);
		g.drawString(Integer.toString(basic), 100, 100);
		g.setColor(Color.green);
		g.drawString("Majic: ", 10, 120);
		g.setColor(Color.black);
		g.drawString(Integer.toString(majic), 100, 120);
		g.setColor(Color.pink);
		g.drawString("Rare: ", 10, 140);
		g.setColor(Color.black);
		g.drawString(Integer.toString(rare), 100, 140);
		g.setColor(Color.red);
		g.drawString("Legendary: ", 10, 160);
		g.setColor(Color.black);
		g.drawString(Integer.toString(legendary), 100, 160);
		
		
	}
	public void addLootRank(Rank in){
		switch(in){
		case broken:
				broken++;
				break;
		case basic:
				basic++;
				break;
		case magic:
				majic++;
				break;
		case rare:
				rare++;
				break;
		case legendary:
				legendary++;
				break;
		}
	}
	
}
