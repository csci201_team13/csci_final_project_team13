package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import Data.Creature;
import Data.Equipment;
import Data.Rank;
import Game.GameManager;
import LoginGui.ClientListener;

public class JClientGui extends JFrame {
	private JPanel base;
	private JPanel left;
	private JPanel mid;
	private JPanel right;
	private JTabbedPane rightTab;

	private JBagPane bp;
	private JChangePlayer cp;
	private JLootPane lt;
	private JSkillTotalPane sp;
	private JPlayerPane pp;
	private JBattlePanel bap;
	private JEnemyPanel ep;
	private JBodyPanel bdp;
	private GUI.JGifPane gp;
	private JSkillActivated sa;
	private JScrollPane scrollBp;
	private JScrollPane scrollBdp;
	private JScrollPane scrollSp;
	private JSettingPane settingPane;

	private JScrollPane scSa;

	private List<Equipment> itEquipment;
	private Creature thisPlayer;
	private GameManager gm;
	private ClientListener mListener;
	
    Point mouseDownCompCoords;

	public JClientGui(Creature player, ClientListener cl) {
		thisPlayer = player;
		mListener = cl;
		setTitle("Endless Battle Beta");
		settingPane = new JSettingPane(mListener, thisPlayer, this);

		ep = new JEnemyPanel();
		bap = new JBattlePanel();
		pp = new JPlayerPane(thisPlayer);
		cp = new JChangePlayer(thisPlayer);

		lt = new JLootPane();
		try {
			gp = new GUI.JGifPane();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sa = new JSkillActivated(thisPlayer.getPassiveSkills(), thisPlayer.getPositiveSkills());
		scSa = new JScrollPane(sa);

		gm = new GameManager(thisPlayer, this);
		settingPane.setGameManager(gm);
		sp = new JSkillTotalPane(thisPlayer, this, gm);

		// System.out.println(thisPlayer.getName());
		bp = new JBagPane(thisPlayer, this, gm);
		bdp = new JBodyPanel(thisPlayer, bp);

		//gm.start();

		base = new JPanel();
		left = new JPanel();
		mid = new JPanel();
		right = new JPanel();

		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
		mid.setLayout(new BoxLayout(mid, BoxLayout.Y_AXIS));

		left.add(pp);
		left.add(bap);

		mid.add(ep);
		mid.add(scSa);
		mid.add(lt);

		rightTab = new JTabbedPane();
		rightTab.setPreferredSize(new Dimension(300, 300));
		scrollSp = new JScrollPane(sp, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollBp = new JScrollPane(bp, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollBdp = new JScrollPane(bdp, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		// scrollSp.setMaximumSize(new Dimension(300,300));
		// scrollSp.setMinimumSize(new Dimension(300,300));
		// scrollBp.setMaximumSize(new Dimension(300,300));
		// scrollBp.setMinimumSize(new Dimension(300,300));
		// scrollBdp.setMaximumSize(new Dimension(300,300));
		// scrollBdp.setMinimumSize(new Dimension(300,300));
		// scrollBp.setBounds(0, 0, 300, 300);
		// rightTab.add("Level Up", cp);
		rightTab.add("Skill", scrollSp);
		rightTab.add("Inventory", scrollBp);
		rightTab.add("Equipped", scrollBdp);
		rightTab.add("Setting", settingPane);

		rightTab.setPreferredSize(new Dimension(410, 600));

		right.setLayout(new BoxLayout(right, BoxLayout.Y_AXIS));
		right.setPreferredSize(new Dimension(410, 800));
		right.add(rightTab);
		right.add(gp);

		add(left, BorderLayout.WEST);
		add(mid, BorderLayout.CENTER);
		add(right, BorderLayout.EAST);

		try {
			final Timer timer = new Timer();
			JFrame centerPanel = new LoginGui.JGifPane(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e)
				{
					makeVisible();
					gm.start();
				    timer.cancel();
				}
			});
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					centerPanel.setVisible(false);
					makeVisible();
					try {
						gm.start();
					} catch (Exception e) {}
				}
			}, 8000);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		setVisible(false);
		setSize(1200, 800);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setUndecorated(true);
		addActions();

		/*
		 * while(true){ pp.repaint(); ep.repaint(); lt.repaint(); sa.repaint();
		 * // bp.update(); // sp.update(); // try { // Thread.sleep(100); // }
		 * catch (InterruptedException e) { // // TODO Auto-generated catch
		 * block // e.printStackTrace(); // } //
		 * 
		 * }
		 */
	}
	public void makeVisible() {
		setVisible(true);
	}
	public JBodyPanel getBody() {
		return bdp;
	}

	public void printLog(String str) {
		bap.printLog(str);
	}

	public void addGold(int input) {
		lt.addGold(input);
	}

	public void addExp(int input) {
		lt.addExp(input);
	}

	public void printAttack(String attacker, String defender, int dmg) {
		bap.printAttack(attacker, defender, dmg);
		refresh();
	}

	public void printYouDie() {
		bap.youDie();
	}

	public void printYouWin(String enemy) {
		bap.youWin(enemy);
	}

	public void printMeet(String name) {
		bap.printMeet(name);
	}

	public void printExp(int Exp) {
		bap.printExp(Exp);
	}

	public void printMoney(int money) {
		bap.printMoney(money);
	}

	public void printLoot(Equipment loot) {
		bap.printLoot(loot);
	}

	public void updateInv() {
		bp.updateInv();
	}

	public void setEnemy(Creature e) {
		ep.setEnemy(e);
	}

	public void printUseSkill(String name, String skill) {
		bap.printUseSkill(name, skill);
	}

	public void updateEquip() {
		bp.updateInv();
		bdp.updateBody();
	}

	public void refresh() {
		pp.repaint();
		ep.repaint();
		lt.repaint();
		// sa.repaint();
	}

	public void skillRefresh() {
		sp.update();
	}

	public void swapImage() {
		try {
			gp.swapImage();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addLootRank(Rank in) {
		lt.addLootRank(in);
	}

	// public static void main(String args[]) {
	// new JClientGui(Creature.generateCreature(Race.Dwarf));
	//
	// }
	
	private void addActions()
	{
		addMouseListener(new MouseListener(){
            public void mouseReleased(MouseEvent e) {
                mouseDownCompCoords = null;
            }
            public void mousePressed(MouseEvent e) {
                mouseDownCompCoords = e.getPoint();
            }
            public void mouseExited(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseClicked(MouseEvent e) {
            }
        });

        addMouseMotionListener(new MouseMotionListener(){
            public void mouseMoved(MouseEvent e) {
            }

            public void mouseDragged(MouseEvent e) {
                Point currCoords = e.getLocationOnScreen();
                setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
            }
        });
	}
}
