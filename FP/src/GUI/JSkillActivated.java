package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Data.Skill;

public class JSkillActivated extends JPanel{
	private Set<Skill> paSkills;
	private Set<Skill> poSkills;
	private Border title;
	public JSkillActivated(Set<Skill> pa, Set<Skill> po){
		title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black),"Activated Skill");
		setBorder(title);
		poSkills = po;
		paSkills = pa;
	}
	
	public void paintComponent(Graphics g){
		Iterator<Skill> it = poSkills.iterator();
		int xLoc = 10;
		int yLoc = 30;
		while(it.hasNext()){
			Skill temp = it.next();
			if(temp.IsActivated()){
				g.drawString(temp.getName() + " is activated", xLoc, yLoc);
				yLoc += 20;
			}
		}
		
		Iterator<Skill> it2 = paSkills.iterator();
		while(it2.hasNext()){
			Skill temp = it2.next();
			if(temp.IsActivated()){
				g.drawString(temp.getName() + " is activated", xLoc, yLoc);
				yLoc += 20;
			}
		}
		
	}
}
