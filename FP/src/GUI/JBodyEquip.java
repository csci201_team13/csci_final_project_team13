package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import Data.Creature;
import Data.Equipment;

public class JBodyEquip extends JPanel{
	private Equipment thisEquipment;
	private Border title;
	private JButton unequip;
	private Creature player;
	private JBodyPanel body;
	private JBagPane bag;
	private JPanel left;
	private JPanel button;
	
	private flowFrame ff;
	private int lines;
	
	public JBodyEquip(Equipment e, Creature p, JBodyPanel s, JBagPane b){
		thisEquipment = e;
		player = p;
		body = s;
		bag = b;
		
		left = new JPanel();
		button = new JPanel();
		
		left.setLayout(new FlowLayout(FlowLayout.LEFT));
		button.setLayout(new FlowLayout(FlowLayout.RIGHT));
		left.setPreferredSize(new Dimension(230,50));
		button.setPreferredSize(new Dimension(130,50));
		
				
		title = BorderFactory.createLineBorder(Color.white);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setPreferredSize(new Dimension(380,60));
		setBorder(title);
		unequip = new JButton("unequip");
		
		JTextArea jta = new JTextArea(thisEquipment.geteType()+ " " +"Level " + thisEquipment.getLevel() + "\n" + thisEquipment.getName());
		jta.setEditable(false);
		jta.setBackground(new Color(238, 238, 238));
		left.add(jta);
		button.add(unequip);
		add(left, BorderLayout.WEST);
		add(button, BorderLayout.EAST);
		
		unequip.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				player.unequip(thisEquipment);
				body.updateBody();
				bag.updateInv();;
			}
			
		});

		JPanel infoJP = new JPanel();
		infoJP.setBorder(BorderFactory.createLineBorder(Color.red));
		String info_1 = "";
		String info_2 = "";
		lines = 0;
		if (e.getHP() > 0) {
			info_1 += "HP:\n";
			info_2 += "+" + e.getHP() + "\n";
			++lines;
		}
		if (e.getMP() > 0) {
			info_1 += "MP:\n";
			info_2 += "+" + e.getMP() + "\n";
			++lines;
		}
		if (e.getPhysicalAttack() > 0) {
			info_1 += "ATK:\n";
			info_2 += "+" + e.getPhysicalAttack() + "\n";
			++lines;
		}
		if (e.getPhysicalDefence() > 0) {
			info_1 += "DEF:\n";
			info_2 += "+" + e.getPhysicalDefence() + "\n";
			++lines;
		}
		if (e.getMagicAttack() > 0) {
			info_1 += "MATK:\n";
			info_2 += "+" + e.getMagicAttack() + "\n";
			++lines;
		}
		if (e.getMagicDefence() > 0) {
			info_1 += "RES:\n";
			info_2 += "+" + e.getMagicDefence() + "\n";
			++lines;
		}
		if (e.getDexterity() > 0) {
			info_1 += "DEX:\n";
			info_2 += "+" + e.getDexterity() + "\n";
			++lines;
		}
		if (e.getLuck() > 0) {
			info_1 += "LUCK:\n";
			info_2 += "+" + e.getLuck() + "\n";
			++lines;
		}
		if (e.getCriticalHit() > 0) {
			info_1 += "CH:\n";
			info_2 += "+" + e.getCriticalHit() + "\n";
			++lines;
		}
		JTextArea infoLabel_1 = new JTextArea(info_1);
		infoLabel_1.setEditable(false);
		infoLabel_1.setBackground(new Color(238,238,238));
		JTextArea infoLabel_2 = new JTextArea(info_2);
		infoLabel_2.setEditable(false);
		infoLabel_2.setBackground(new Color(238,238,238));
		infoJP.setLayout(new GridLayout(1, 2));
		infoJP.add(infoLabel_1);
		infoJP.add(infoLabel_2);
		infoJP.setSize(110, lines*20);
		ff = new flowFrame(infoJP);


		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				for(flowFrame f : flowFrame.frameList)
				{
					f.setVisible(false);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {				
				Point parentLocation = e.getComponent().getLocationOnScreen();
				double y = parentLocation.getY();
				if (y < 0) y = 10;
				parentLocation.setLocation(parentLocation.getX() - ff.getWidth(), y);
				ff.setLocation(parentLocation);
				ff.repaint();
				ff.revalidate();
				ff.setVisible(true);
				
			}
		});
	}
	
	
}
