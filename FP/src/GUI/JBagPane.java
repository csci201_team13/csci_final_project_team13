package GUI;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JPanel;

import Data.Creature;
import Data.Equipment;
import Game.GameManager;

public class JBagPane extends JPanel {
	private Equipment[] equipHave;
	private Vector<Equipment> equipBag;
//	private JEquipPane[] equipPane;
	private Vector<JEquipPane> equipPane;
	private JClientGui client;
	private GameManager gamem;
	

	private Creature thisPlayer;
	
	public JBagPane(Creature p, JClientGui b, GameManager gm){
		thisPlayer = p;
		client = b;
		gamem = gm;
		equipHave = thisPlayer.getEquipments();
		equipBag = thisPlayer.getInventory();
//		equipPane = new JEquipPane[equipBag.size()];
		equipPane = new Vector<JEquipPane>();
		GridBagConstraints gbc = new GridBagConstraints();
		int y = 0;
		int num = equipBag.size();
		if(num < 4)
			num = 4;
		
		setLayout(new GridLayout(num,1));
		for(int i = 0; i < equipBag.size(); i++){
			gbc.anchor = GridBagConstraints.NORTH;
			equipPane.add(new JEquipPane(equipBag.get(i), thisPlayer, this, client, gamem));
			gbc.gridx = 0;
			gbc.gridy = y;
			add(equipPane.get(i));
			y++;
		}
		
		
		
		if(equipBag.size() < 4)
		{
			int offset = 4 - equipBag.size();
			for(int i = 0; i < offset; i++)
			{
				add(new JPanel());
			}
		}
//		if(equipBag.size() > 0)
//			setPreferredSize(new Dimension(400,(equipPane.get(0).getPreferredSize().height) * equipBag.size()));
//		else
//			setPreferredSize(new Dimension(400, 70));
	}
	
	
	
	public synchronized void updateInv(){
		removeAll();
		equipHave = thisPlayer.getEquipments();
		equipBag = thisPlayer.getInventory();
		equipPane = new Vector<JEquipPane>();
		GridBagConstraints gbc = new GridBagConstraints();
		int y = 0;
		
		int num = equipBag.size();
		if(num < 4)
			num = 4;
		
		setLayout(new GridLayout(num, 1));
		for(int i = 0; i < equipBag.size(); i++){
			gbc.anchor = GridBagConstraints.NORTH;
			equipPane.add(new JEquipPane(equipBag.get(i), thisPlayer, this, client,gamem));
			gbc.gridx = 0;
			gbc.gridy = y;
			add(equipPane.get(i));
			y++;
		}
		if(equipBag.size() < 4)
		{
			int offset = 4 - equipBag.size();
			for(int i = 0; i < offset; i++)
			{
				add(new JPanel());
			}
		}
		
//		if(equipBag.size() > 0)
//			setPreferredSize(new Dimension(400,(equipPane.get(0).getPreferredSize().height) * equipBag.size()));
//		else
//			setPreferredSize(new Dimension(400, 70));
		revalidate();
		repaint();
	
	}
	
	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}
}
