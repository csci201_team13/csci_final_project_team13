
package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import Data.Creature;
import Data.Equipment;
import Data.Rank;
import Game.GameManager;
import library.FontLibrary;

public class JEquipPane extends JPanel {
	private JButton equip;
	private JButton sell;
	private Equipment thisEquipment;
	private Creature thisPlayer;
	private String name;
	private Rank rank;
	private JPanel buttonPane;
	private JLabel nameLabel;
	private JLabel rankLabel;
	private JLabel typeLabel;
	private JPanel basePanel;

	private Border title;

	private int price;
	private JBagPane bag;
	private JBodyPanel bp;
	private JButton upgrade;
	private GameManager gamem;
	private JPanel left;

	private flowFrame ff;
	private int lines;

	public JEquipPane(Equipment e, Creature player, JBagPane b, JClientGui bp, GameManager gm) {
		title = BorderFactory.createLineBorder(Color.white);
		setBorder(title);
		bag = b;

		gamem = gm;

		left = new JPanel();

		setLayout(new FlowLayout(FlowLayout.LEFT));
		thisEquipment = e;
		thisPlayer = player;
		name = e.getName();
		rank = e.getRank();
		rankLabel = new JLabel(rank.toString());
		typeLabel = new JLabel(e.geteType().toString());

		nameLabel = new JLabel(name);
		buttonPane = new JPanel();
		basePanel = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
		equip = new JButton("Equip");
		sell = new JButton("Sell(" + String.format("%d Gold)", e.getPrice()));
		upgrade = new JButton("Upgrade(" + String.format("%.2f", player.getSuccessRate(e) * 100) + "%)");

		buttonPane.setSize(new Dimension(170, 130));

		buttonPane.add(equip);
		buttonPane.add(sell);
		buttonPane.add(upgrade);

		setAlignmentX(Component.LEFT_ALIGNMENT);

		left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
		left.setPreferredSize(new Dimension(190, 130));
		left.add(new JLabel("Level: " + thisEquipment.getLevel()));
		left.add(typeLabel);
		left.add(rankLabel);
		left.add(nameLabel);

		add(left, BorderLayout.WEST);
		add(buttonPane, BorderLayout.CENTER);

		// setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		setPreferredSize(new Dimension(380, 140));

		price = thisEquipment.getPrice();
		// buttonPane.add(comp)
		equip.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				equipIt();
				bag.updateInv();
				bp.getBody().updateBody();
			}

		});

		sell.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				sell();
				bag.updateInv();
			}

		});

		upgrade.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				gamem.upgradeEquip(thisEquipment);
			}

		});

		JPanel infoJP = new JPanel();
		infoJP.setBorder(BorderFactory.createLineBorder(Color.red));
		String info_1 = "";
		String info_2 = "";
		lines = 0;
		if (e.getHP() > 0) {
			info_1 += "HP:\n";
			info_2 += "+" + e.getHP() + "\n";
			++lines;
		}
		if (e.getMP() > 0) {
			info_1 += "MP:\n";
			info_2 += "+" + e.getMP() + "\n";
			++lines;
		}
		if (e.getPhysicalAttack() > 0) {
			info_1 += "ATK:\n";
			info_2 += "+" + e.getPhysicalAttack() + "\n";
			++lines;
		}
		if (e.getPhysicalDefence() > 0) {
			info_1 += "DEF:\n";
			info_2 += "+" + e.getPhysicalDefence() + "\n";
			++lines;
		}
		if (e.getMagicAttack() > 0) {
			info_1 += "MATK:\n";
			info_2 += "+" + e.getMagicAttack() + "\n";
			++lines;
		}
		if (e.getMagicDefence() > 0) {
			info_1 += "RES:\n";
			info_2 += "+" + e.getMagicDefence() + "\n";
			++lines;
		}
		if (e.getDexterity() > 0) {
			info_1 += "DEX:\n";
			info_2 += "+" + e.getDexterity() + "\n";
			++lines;
		}
		if (e.getLuck() > 0) {
			info_1 += "LUCK:\n";
			info_2 += "+" + e.getLuck() + "\n";
			++lines;
		}
		if (e.getCriticalHit() > 0) {
			info_1 += "CH:\n";
			info_2 += "+" + e.getCriticalHit() + "\n";
			++lines;
		}
		JTextArea infoLabel_1 = new JTextArea(info_1);
		infoLabel_1.setEditable(false);
		infoLabel_1.setBackground(new Color(238,238,238));
		infoLabel_1.setLineWrap(true);
		JTextArea infoLabel_2 = new JTextArea(info_2);
		infoLabel_2.setEditable(false);
		infoLabel_2.setBackground(new Color(238,238,238));
		infoLabel_2.setLineWrap(true);
		infoJP.setLayout(new GridLayout(1, 2));
		infoJP.add(infoLabel_1);
		infoJP.add(infoLabel_2);
		infoJP.setSize(110, lines*20);
		ff = new flowFrame(infoJP);
		
		


		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				for(flowFrame f : flowFrame.frameList)
				{
					f.setVisible(false);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				Point parentLocation = e.getComponent().getLocationOnScreen();
				double y = parentLocation.getY();
				if (y < 0) y = 10;
				parentLocation.setLocation(parentLocation.getX() - ff.getWidth(), y);
				ff.setLocation(parentLocation);
				ff.repaint();
				ff.revalidate();
				ff.setVisible(true);
			}
		});
	}

	public void equipIt() {
		thisPlayer.equip(thisEquipment);

	}

	public void sell() {
		thisPlayer.addGold(price);
		thisPlayer.getInventory().remove(thisEquipment);
	}

	public void setEquip() {

	}

	public void paintComponent(Graphics g) {

	}
}
