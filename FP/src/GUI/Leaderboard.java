package GUI;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Data.Creature;

public class Leaderboard extends JDialog
{
	public Leaderboard(Vector<Creature> creatures)
	{
		String[] columnNames = { "Rank", "Name", };
		Object[][] data = new Object[creatures.size()][2];
		for (int i = 0; i < creatures.size(); i++)
		{
			data[i][0] = i;
			data[i][1] = creatures.get(i).getName();
		}
		final JTable table = new JTable(data, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setEnabled(false);
		setSize(new Dimension(300, 400));
		add(scrollPane);
		setVisible(true);
	}
}
