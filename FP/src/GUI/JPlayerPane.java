package GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Data.Creature;
import Data.Race;
import library.FontLibrary;

public class JPlayerPane extends JPanel {
	private Creature thisPlayer = null;
	private Race race;
	private String name;
	private int age;
	private int lev;
	private int hpNow;
	private int mpNow;
	private int hpMax;
	private int mpMax;
	private int gold;
	private int str;
	private int dex;
	private int luck;
	private int AP;
	private int CP;
	private Border title;
	private JLabel label_1;
	private JLabel label_1_1;
	private JLabel label_2;
	private JLabel label_2_1;

	public JPlayerPane(Creature p) {
		thisPlayer = p;
		title = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Player");
		setBorder(title);
		if (thisPlayer != null) {
			race = p.getRace();
			name = p.getName();
			AP = p.getPhysicalAttack();
			CP = p.getMagicAttack();
			hpMax = p.getHP();
			hpNow = p.getHPCurrent();
			mpMax = p.getMP();
			mpNow = p.getMPCurrent();
			gold = p.getGold();
			dex = p.getDexterity();
			lev = p.getLevel();
			luck = p.getLuck();
		}

		setLayout(new GridLayout(1, 4));
		label_1 = new JLabel("<html>Name:<br>Level:<br>HP:<br>MP:<br>Race:<br>DEX:<br>Gold:</html>");
		label_1_1 = new JLabel("<html>" + thisPlayer.getName() + "<br>" + thisPlayer.getLevel() + "<br>"
				+ thisPlayer.getHPCurrent() + "<br>" + thisPlayer.getMPCurrent() + "<br>" + thisPlayer.getRace() + "<br>"
				+ thisPlayer.getDexterityBuff() + "<br>" + thisPlayer.getGold() + "</html>");
		label_2 = new JLabel("<html>LUCK:<br>ATK:<br>MATK:<br>DEF:<br>RES:<br>AP:<br>EXP:</html>");
		label_2_1 = new JLabel("<html>" + thisPlayer.getLuckBuff() + "<br>" + thisPlayer.getPhysicalAttackBuff()
				+ "<br>" + thisPlayer.getMagicAttackBuff() + "<br>" + thisPlayer.getPhysicalDefenceBuff() + "<br>"
				+ thisPlayer.getMagicDefenceBuff() + "<br>" + thisPlayer.getAP() + "<br>" + thisPlayer.getExp() + "/"
				+ thisPlayer.getExpNeeded() + "</html>");

		add(label_1);
		add(label_1_1);
		add(label_2);
		add(label_2_1);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		label_1_1.setText("<html>" + thisPlayer.getName() + "<br>" + thisPlayer.getLevel() + "<br>" + thisPlayer.getHPCurrent()
				+ "<br>" + thisPlayer.getMPCurrent() + "<br>" + thisPlayer.getRace() + "<br>" + thisPlayer.getDexterityBuff()
				+ "<br>" + thisPlayer.getGold() + "</html>");
		label_2_1.setText("<html>" + thisPlayer.getLuckBuff() + "<br>" + thisPlayer.getPhysicalAttackBuff() + "<br>"
				+ thisPlayer.getMagicAttackBuff() + "<br>" + thisPlayer.getPhysicalDefenceBuff() + "<br>"
				+ thisPlayer.getMagicDefenceBuff() + "<br>" + thisPlayer.getAP() + "<br>" + thisPlayer.getExp() + "/"
				+ thisPlayer.getExpNeeded() + "</html>");
	}
	/*
	 * public void paintComponent(Graphics g){ super.paintComponent(g);
	 * g.drawString("Name: " + thisPlayer.getName(), 10, 40); g.drawString(
	 * "Level: " + thisPlayer.getLevel(), 10, 60); g.drawString("HP: " +
	 * thisPlayer.getHPCurrent(), 10, 80); g.drawString("MP: " +
	 * thisPlayer.getMPCurrent(), 10, 100); g.drawString("Gold: " +
	 * thisPlayer.getGold(), 10, 120);
	 * 
	 * g.drawString("Race: " + thisPlayer.getRace(), 220, 40); g.drawString(
	 * "DEX: " + thisPlayer.getDexterity(), 220, 60); g.drawString("LUCK: " +
	 * thisPlayer.getLuck(), 220,80); g.drawString("AP: "
	 * +thisPlayer.getPhysicalAttack(),220 , 100); g.drawString("CP: " +
	 * thisPlayer.getMagicAttack(), 220, 120); }
	 */
}
