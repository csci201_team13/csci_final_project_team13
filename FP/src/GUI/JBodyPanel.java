package GUI;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import Data.Creature;
import Data.Equipment;

public class JBodyPanel extends JPanel {
	private Equipment[] equipped;
	private JBodyEquip[] s;
	private Creature thisPlayer;
	private JBagPane bag;
	
	public JBodyPanel(Creature p, JBagPane b){
		thisPlayer = p;
		bag = b;
		equipped = thisPlayer.getEquipments();
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(380,800));
		if(equipped.length == 0){
			s = new JBodyEquip[0];
		}else{
			s = new JBodyEquip[equipped.length];
		}
		for(int i = 0; i < s.length; i++){
			if(equipped[i] != null){
				s[i] = new JBodyEquip(equipped[i], thisPlayer,this, bag);
				add(s[i]);
			}
		}
	}
	
	public void updateBody(){
		removeAll();
		equipped = thisPlayer.getEquipments();
		setLayout(new FlowLayout());
		setPreferredSize(new Dimension(400,50));
		if(equipped.length == 0){
			s = new JBodyEquip[0];
		}else{
			s = new JBodyEquip[equipped.length];
		}
		for(int i = 0; i < s.length; i++){
			if(equipped[i] != null){
				s[i] = new JBodyEquip(equipped[i], thisPlayer,this, bag);
				add(s[i]);
			}
		}
		revalidate();
		repaint();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
	}
}
