package Data;
import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

import Server.AWSServer;

public class mysql {
	private static void generateCreatureData() {
		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/fp";
			String username = "root";
			String password = "root";

			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, username, password);

			PreparedStatement ps = conn.prepareStatement("SELECT * from fp.growrate;");
			ResultSet grs = ps.executeQuery();
			int id = 1;
			while (grs.next()) {
				int race = grs.getInt("race");
				double bHP = grs.getInt("bHP");
				double gHP = grs.getDouble("gHP");
				double bMP = grs.getInt("bMP");
				double gMP = grs.getDouble("gMP");
				double bpA = grs.getInt("bPhysicalAttack");
				double gpA = grs.getDouble("gPhysicalAttack");
				double bpD = grs.getInt("bPhysicalDefence");
				double gpD = grs.getDouble("gPhysicalDefence");
				double bmA = grs.getInt("bMagicAttack");
				double gmA = grs.getDouble("gMagicAttack");
				double bmD = grs.getInt("bMagicDefence");
				double gmD = grs.getDouble("gMagicDefence");
				double bdex = grs.getInt("bDexterity");
				double gdex = grs.getDouble("gDexterity");
				double bluck = grs.getInt("bLuck");
				double gluck = grs.getDouble("gLuck");
				double bch = grs.getDouble("bCriticalHit");
				double gch = grs.getDouble("gCriticalHit");
				double bexp = grs.getInt("bexp");
				double gexp = grs.getDouble("gexp");

				int level = 1;
				for (int i = 0; i < 100; ++i) {
					PreparedStatement insertPS = conn.prepareStatement("INSERT INTO fp.creature VALUES (" + id + ","
							+ level + "," + race + "," + bHP + "," + bMP + "," + bpA + "," + bpD + "," + bmA + "," + bmD
							+ "," + bdex + "," + bluck + "," + bch + "," + bexp + ");");
					insertPS.execute();

					++id;
					++level;

					bHP *= gHP;
					bMP *= gMP;
					bpA *= gpA;
					bpD *= gpD;
					bmA *= gmA;
					bmD *= gmD;
					bdex *= gdex;
					bluck *= gluck;
					bch *= gch;
					bexp *= gexp;
				}
			}

			PreparedStatement sps = conn.prepareStatement("SELECT * FROM creature;");
			ResultSet rs = sps.executeQuery();
			while (rs.next()) {
				System.out.println("CreatureID:" + rs.getInt(1) + "\n" + "level:" + rs.getInt(2) + "\n" + "race:"
						+ rs.getInt(3) + "\n" + "HP:" + rs.getInt(4) + "\n" + "MP:" + rs.getInt(5) + "\n" + "pAttack:"
						+ rs.getInt(6) + "\n" + "pDefence:" + rs.getInt(7) + "\n" + "mAttack:" + rs.getInt(8) + "\n"
						+ "mDefence:" + rs.getInt(9) + "\n" + "dex:" + rs.getInt(10) + "\n" + "luck:" + rs.getInt(11)
						+ "\n" + "ch:" + rs.getDouble(12) + "\nexp:" + rs.getInt(13) + "\n\n");
			}
			rs.close();
			conn.close();
		} catch (SQLException sqle) {
			System.out.println("SQLException: " + sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException: " + cnfe.getMessage());
		}
	}

	private static String valueList(String[] vList) {
		StringBuffer l = new StringBuffer();
		for (String s : vList) {
			l.append(s);
			l.append(',');
		}
		return l.substring(0, l.length() - 1);
	}
/*
	private static void generateEquipmentData() {
		try {
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/fp";
			String username = "root";
			String password = "root";

			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url, username, password);

			String[] vList = { "equipmentID", "typeID", "rankID", "MinHP", "MaxHP", "MinMP", "MaxMP",
					"MinPhysicalAttack", "MaxPhysicalAttack", "MinPhysicalDefence", "MaxPhysicalDefence",
					"MinMagicAttack", "MaxMagicAttack", "MinMagicDefence", "MaxMagicDefence", "MinDexterity",
					"MaxDexterity", "MinLuck", "MaxLuck", "MinCriticalHit", "MaxCriticalHit", "growRate" };

			int equipmentID = 1;
			// 4 types, 5 ranks
			for (int i = 0; i < 4; ++i) {
				int typeID = i;
				
				int MinHP;
				int MaxHP;
				int MinMP;
				int MaxMP;
				int MinPhysicalAttack;
				int MaxPhysicalAttack;
				int MinPhysicalDefence;
				int MaxPhysicalDefence;
				int MinMagicAttack;
				int MaxMagicAttack;
				int MinMagicDefence;
				int MaxMagicDefence;
				int MinDexterity;
				int MaxDexterity;
				int MinLuck;
				int MaxLuck;
				int MinCriticalHit;
				int MaxCriticalHit;
				int growRate;
				for (int j = 0; j < 5; ++j) {
					int rankID = j;
					
					String sql = "INSERT INTO equipment VALUES (" + valueList(vList) + ");";
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.execute();
				}
			}
			

		} catch (SQLException sqle) {
			System.out.println("SQLException: " + sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println("ClassNotFoundException: " + cnfe.getMessage());
		}
	}
*/


	private static void test() {
		// random equipment
		for (int i = 0; i < 5; ++i) {
			Equipment e = Equipment.generateRandomEquipment();
			System.out.println(e.toString());
			for (int j = 0; j < 10; ++j) {
				e.upgrade();
				System.out.print("\nupgraded");
				System.out.println(e.toString());
			}
			
		}
		// random creature
		for (int i = 0; i < 1; ++i) {
			Creature c = Creature.generateRandomCreature();
			System.out.println(c.toString());
			for (int j = 0; j < 100; ++j) {
				c.upgrade();
				System.out.print("\nupgraded");
				System.out.println(c.toString());
			}
		}
		// creature equips/unequips equipment
		Creature c = Creature.generateRandomCreature();
		for (int i = 0; i < 10; ++i) {
			Equipment e = Equipment.generateRandomEquipment();
			System.out.println(c.toString());
			System.out.println(e.toString());
			
			c.equip(e);
			System.out.println(c.toString());
			c.unequip(e);
			System.out.println(c.toString());
		}
		for (int i = 0; i < 10; ++i) {
			Equipment e = Equipment.generateRandomEquipment();
			System.out.println(c.toString());
			System.out.println(e.toString());
			
			c.equip(e);
			System.out.println(c.toString());
		}
	}
	
	static public void main(String[] args) {
		// generateCreatureData();
		//generateEquipmentData();
		
		//Equipment.printData();
		
		
		
		//test();
		//AWSServer server = new AWSServer();
		/*
		Equipment e = Equipment.generateRandomEquipment();
		System.out.println(e.toString());
		e.upgrade();
		System.out.println(e.toString());
		*/
		Race[] race = Race.values();
		for (int i = 0; i < 5; ++i) {
			Creature c= Creature.generateCreature(race[i]);
			System.out.println(c.toString());
		}
		
		
		
		
	}
}
