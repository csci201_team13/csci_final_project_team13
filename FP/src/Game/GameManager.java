package Game;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import Data.*;
import GUI.JClientGui;

public class GameManager extends Thread {
	private Creature player;
	private Creature enemy;
	private int turn;
	private ArrayList<Creature> BossList;
	private int map;
	private Random random;
	private boolean isBoss;
	private int bossIndex;
	private JClientGui gui;
	private boolean useCombo;

	private boolean pvpFlag;

	public GameManager(Creature player, JClientGui jcg) {
		pvpFlag = false;

		gui = jcg;
		this.player = player;
		turn = 0; // 0 is player, 1 is the enemy
		BossList = new ArrayList<Creature>();
		this.map = 0;
		random = new Random();
		isBoss = false;
		useCombo = false;
		generateEnemy();
		gui.printMeet(enemy.getName());
		System.out.println(gui);
	}

	private void checkBossList() {
		// Check to see if all the boss has died
		// generate a new one if so
		for (int i = 0; i < BossList.size(); i++) {
			if (BossList.get(i).getHPCurrent() > 0)
				return;
		}
		BossList.clear();
		generateBossList();
	}

	private void generateEnemy() {
		if (pvpFlag) {
			//pvpFlag = false;
			enemy = Creature.getPVPPlayer();
		} else {
			isBoss = (random.nextInt() % 10) > 8;
			if (isBoss) {
				if (BossList.isEmpty())
					generateBossList();
				bossIndex = random.nextInt(BossList.size());
				enemy = BossList.get(bossIndex);
			} else
				enemy = generateNormalEnemy();
		}
		// System.out.println(gui);
		gui.setEnemy(enemy);
	}

	private void generateBossList() {
		// Generate 10 boss enemies at a time
		// regenerate it when used up
		for (int i = 0; i < 10; i++) {
			Creature boss = Creature.generateRandomCreature();
			boss.bossbuff();
			BossList.add(boss);
		}
	}

	private Creature generateNormalEnemy() {
		Creature e = Creature.generateRandomCreature();
		// enemy level is bounded with player level
		for (int i = 0; i < player.getLevel(); ++i) {
			e.upgrade();
		}
		
		for (int i = 0; i < (5 - map); i++)
			e.debuff();
		return e;
	}

	private Set<Skill> shouldSkill(Creature c, boolean isPositive) {
		Set<Skill> playerSkills = (isPositive) ? c.getPositiveSkills() : c.getPassiveSkills();
		if (playerSkills.isEmpty())
			return null;
		else {
			// Check if no skill is activated
			boolean hasOneActivated = false;
			for (Skill s : playerSkills) {
				if (s.IsActivated())
					hasOneActivated = true;
			}
			if (!hasOneActivated)
				return null;
			if (c.getMPCurrent()<34) return null;
			if ((random.nextInt() % 10) >= 5) {
				c.setMPCurrent(c.getMPCurrent()-34);
				return playerSkills;
			}
			else
				return null;
		}
	}

	private int getNormalAttack(Creature attacker, Creature defender) {
		int cause = attacker.getPhysicalAttackBuff();
		int receive = defender.getPhysicalDefenceBuff();
		
		double actuallDamage=cause*100/(receive+100);
		Random rand=new Random();
		
		actuallDamage=actuallDamage*0.85+actuallDamage*rand.nextFloat()*0.30;
		//System.out.println(cause+" "+receive+" "+actuallDamage);
		return Math.max((int)actuallDamage * 4,1);
	}

	private int getMagicAttack(Creature attacker, Creature defender) {
		int cause = attacker.getMagicAttackBuff();
		int receive = defender.getMagicDefenceBuff();
		double actuallDamage=cause*100/(receive+100);
		Random rand=new Random();
		
		actuallDamage=actuallDamage*0.85+actuallDamage*rand.nextFloat()*0.30;
		return Math.max((int)actuallDamage * 4,1);
	}

	private int getSkillAttack(Creature attacker, Creature defender, Skill s) {
		// Get the cause of the skill before defense
		int cause = 0;
		gui.printUseSkill(attacker.getName(), s.getSkill().toString());
		switch (s.getSkill()) {
		case Smash:
			cause = getNormalAttack(attacker, defender) * 2;
			// gui.swapImage();
			break;
		case Combo:
			cause = getNormalAttack(attacker, defender);
			useCombo = true;
			// gui.swapImage();
			break;
		case IceBolt:
		case FireBolt:
			cause = getMagicAttack(attacker, defender) * 2;
			// gui.swapImage();
			break;
		default:
		}
		return cause;
	}

	private void harm(Creature attacker, Creature defender, int amount) {
		defender.setHPCurrent(Math.max(defender.getHPCurrent() - amount, 0));
		gui.printAttack(attacker.getName(), defender.getName(), Math.max(amount, 0));
		if (useCombo) {
			defender.setHPCurrent(Math.max(defender.getHPCurrent() - amount, 0));
			gui.printAttack(attacker.getName(), defender.getName(), Math.max(amount, 0));
			useCombo = false;
		}
	}

	private void useDefenseSkill(Creature attacker, Creature defender, Skill s, int beforeCause) {
		gui.printUseSkill(defender.getName(), s.getSkill().toString());
		switch (s.getSkill()) {
		case CouterBack:
			harm(attacker, attacker, beforeCause);
			break;
		case Defense:
			int amount = (int) Math.round(beforeCause * 0.8);
			harm(attacker, defender, amount);
			break;
		default:
		}
	}

	private void makeTurn() {
		if (turn == 0)
			battle(player, enemy);
		else
			battle(enemy, player);
		switch (checkWin()) {
		case 0:
			gui.printYouDie();
			if (isBoss) {
				generateEnemy();
				bossIndex = -1;
			}
			player.resurrect();
			turn = 0;
			break;
		case 1:
			gui.printYouWin(enemy.getName());
			if (isBoss) {
				BossList.remove(bossIndex);
				bossIndex = -1;
			}
			reward();
			generateEnemy();
			turn = 0;
			break;
		default:
			turn = 1 - turn;
		}
	}

	private void battle(Creature attacker, Creature defender) {
		// First getting the cause before defense
		int beforeCause = 0;
		Set<Skill> attackSkills = shouldSkill(attacker, true);
		if (attackSkills != null) {
			Skill selectedSkill = null;
			for (Skill s : attackSkills) {
				if (s.IsActivated()) {
					selectedSkill = s;
					break;
				}
			}
			beforeCause = getSkillAttack(attacker, defender, selectedSkill);
			gui.swapImage();
		} else
			beforeCause = getNormalAttack(attacker, defender);
		gui.swapImage();
		// Then check the how the defender reacts
		Set<Skill> defendSkills = shouldSkill(defender, false);
		if (defendSkills != null) {
			Skill selectedSkill = null;
			for (Skill s : defendSkills) {
				if (s.IsActivated()) {
					selectedSkill = s;
					break;
				}
			}
			useDefenseSkill(attacker, defender, selectedSkill, beforeCause);
			gui.swapImage();
		} else
			harm(attacker, defender, beforeCause);
		gui.swapImage();
	}

	private int checkWin() {
		if (player.getHPCurrent() <= 0)
			return 0; // Indicating player death
		else if (enemy.getHPCurrent() <= 0)
			return 1; // Indicating player victory
		else
			return 2; // Indicating no one wins
	}

	private void reward() {
		int exp = 100;
		player.addExp(exp/player.getLevel());
		gui.addExp(exp/player.getLevel());
		EType EQUIPTYPE = EType.values()[random.nextInt(EType.values().length)];
		Rank EQUIPRANK = Rank.values()[random.nextInt(Rank.values().length)];
		// Generate loots and money, then a new enemy
		Equipment e = Equipment.generateEquipment(EQUIPRANK, EQUIPTYPE);
		int money = genMoney(map);
		player.setGold(money + player.getGold());
		Random rand=new Random();
		//System.out.println(rand.nextFloat());
		if((player.getInventory().size() < 50)&&(rand.nextFloat()>0.75))
			{player.addEquipment(e);
		
		gui.printLoot(e);}
		else player.addGold(e.getPrice());
		gui.updateInv();

		gui.addLootRank(EQUIPRANK);
		gui.addGold(money);
		gui.printExp(exp/player.getLevel());
		gui.printMoney(money);
		
	}

	private int genMoney(int currMap) {
		return 100;
	}

	public void upgradeEquip(Equipment e) {
		if (player.getGold() < e.getUpgradeMoney()) {
			gui.printLog("Not enough money to upgrade equipment " + e.getName() + "\n");
			return;
		}

		double rate1 = player.getSuccessRate(e);
		double rate2 = Math.abs(random.nextDouble());
		if (rate1 >= rate2) {
			gui.printLog("Upgrade succeeded!\n");
			player.setGold(player.getGold() - e.getUpgradeMoney());
			e.upgrade();
		} else {
			gui.printLog("Upgrade failed!\n");
			player.setGold(player.getGold() - e.getUpgradeMoney());
			player.getInventory().remove(e);
		}
		gui.updateEquip();
	}

	public void upgradeSkill(Skill s) {
		if (player.getAP() < s.getUpgradeAP()) {
			gui.printLog("Not enough ability point to upgrade skill " + s.getName());
			return;
		}

		s.upgrade();
		// gui.skillRefresh();
		player.comsumeAP(s.getUpgradeAP());
		gui.skillRefresh();
		gui.refresh();
	}

	public void setPvpFlag(boolean pvpFlag) {
		this.pvpFlag = pvpFlag;
	}

	@Override
	public void run() {
		while (true) {
			makeTurn();
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	
	public Creature getPlayer()
	{
		return player;
	}
}