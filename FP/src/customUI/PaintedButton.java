package customUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;


public class PaintedButton extends JButton
{
	private static final long serialVersionUID = 7074393176317490987L;

	private Image		toDraw;
	private final Image	mUp;
	private final Image	mDown;


	public PaintedButton(String name, Image inUp, Image inDown, int inFontSize)
	{
		super(name);
		toDraw = mUp = inUp;
		mDown = inDown;

		addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent arg0)
			{
				toDraw = mDown;
				setForeground(Color.GRAY);
			}

			@Override
			public void mouseReleased(MouseEvent arg0)
			{
				toDraw = mUp;
				setForeground(Color.BLACK);

			}
		});

		setOpaque(true);
		setBackground(new Color(0, 0, 0, 0));
	}

	public void paint(Graphics g)
	{

		g.drawImage(toDraw, 0, 0, getWidth(), getHeight(), null);

		setBorder(BorderFactory.createEmptyBorder(10, 18, 10, 10));
		setContentAreaFilled(false);

		super.paint(g);
	}

	// @Override
	// protected void paintComponent(Graphics g) {
	// g.drawImage(toDraw, 0, 0, getWidth(), getHeight(), null);
	//// g.setFont(FontLibrary.getFont("fonts/kenvector_future_thin.ttf",
	// Font.PLAIN, mFontSize));
	// super.paintComponent(g);
	// }
	//
	// @Override
	// protected void paintBorder(Graphics g) {
	// //paint no border
	// }

}
